package lms.view.dialog;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import lms.controller.RemoveSingleController;
import lms.model.Holding;
import lms.view.AppFrame;

public class RemoveSingleDialog extends JDialog
{
   
   // creates a dialog box to confirm you want to remove just one holding.
   // is created when a holding in the content panel is double-clicked by the user
   
   
   private static final long serialVersionUID = 1L; // default added to remove warning
   private Holding holding;
   private AppFrame frame;
   private JLabel query;
   private JButton confirm, cancel;


   // constructor to create the dialog box
   
   public RemoveSingleDialog (Holding holding, AppFrame frame) {
      
      super();
      this.holding = holding;
      this.frame = frame;
      
      this.setTitle("Remove Holding");
      this.setLayout (new BorderLayout());
      
      query = new JLabel ("Are you sure you want to remove holding?");
      
      this.add(query, BorderLayout.NORTH);
      this.add(insertInfo(this.holding), BorderLayout.CENTER);
      this.add(insertButtons(), BorderLayout.SOUTH);
      
      
      setVisible(true);
      setSize (300, 200);
      setResizable (false);
      setLocationRelativeTo (frame);
      
   }
   
   public JPanel insertInfo (Holding holding) {
      
      JPanel pane = new JPanel();
            
      pane.setLayout(new GridLayout(2, 1));
      
      pane.add(new JLabel ("Holding ID: " + holding.getCode()));
      pane.add(new JLabel ("Holding Title: " + holding.getTitle()));
      
      return pane;
      
   }
   
   public JPanel insertButtons () {
      
      JPanel pane = new JPanel();
      
      pane.setLayout(new FlowLayout());
      
      confirm = new JButton ("Confirm");
      confirm.addActionListener (new RemoveSingleController(this, holding));
      
      cancel = new JButton ("Cancel");
      cancel.addActionListener (new RemoveSingleController(this, holding));
      
      pane.add(confirm);
      pane.add(cancel);
      
      return pane;
      
   }
   
   public AppFrame getFrame() {
      
      return this.frame;
      
   }

}
