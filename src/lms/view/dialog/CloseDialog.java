package lms.view.dialog;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.*;

import lms.controller.CloseController;
import lms.view.AppFrame;

public class CloseDialog extends JDialog
{

   // dialog box created when the user attempts to close the application from
   // either the window close button or the menu exit option
   
   
   private static final long serialVersionUID = 1L; // default added to remove warning
   private JButton confirm, cancel;
   private JLabel query;
   private AppFrame frame;
   
   
   // constructor for initial dialog setup
   
   public CloseDialog (AppFrame frame) {
      
      this.frame = frame;
      
      query = new JLabel ("Are you sure you want to Exit?");
      
      setTitle ("Confirm Close");
      setLayout (new BorderLayout());
      
      add (query, BorderLayout.NORTH);
      add (addButtons(), BorderLayout.CENTER);
      
      
      setVisible(true);
      setSize (250, 100);
      setResizable (false);
      setLocationRelativeTo (frame);
      
//      validate();
      
   }
   
   public JPanel addButtons() {
      
      JPanel panel = new JPanel ();
      
      setLayout (new FlowLayout ());

      
      confirm = new JButton ("Yes");
      confirm.addActionListener (new CloseController (this));
      
      cancel = new JButton ("No");
      cancel.addActionListener (new CloseController (this));
      
      panel.add(confirm);
      panel.add(cancel);
      
      return panel;
      
   }
   
   public AppFrame getFrame () {
      
      return this.frame;
      
   }
   
}
