package lms.view.dialog;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import lms.controller.RemoveHoldingController;
import lms.model.Holding;
import lms.view.AppFrame;

public class RemoveHoldingDialog extends JDialog
{
   
   // dialog box created when the user requests to remove a holding using either the
   // toolbar button or the menu option
   
   
   private static final long serialVersionUID = 1L;

   private AppFrame frame;
   
   private JButton confirm, cancel;
   private JLabel query;
   
   // Hopfully this works. while working across computers this was temperamental depending
   // on what JRE was being used. Will not work with JRE 1.6, but will with 1.7 or above
   private JList<String> select;
   
   
   // constructor for the initial setup of the dialog
   
   public RemoveHoldingDialog (AppFrame frame) {
      
      super();
      this.frame = frame;
      
      this.setLayout (new BorderLayout());
      this.setTitle("Remove Holdings");
      
      this.query = new JLabel ("Select Holdings to Remove");
      
      this.add(query, BorderLayout.NORTH);
      this.add(itemList(), BorderLayout.CENTER);
      this.add(addButtons(), BorderLayout.SOUTH);

      
      setVisible(true);
      setSize (800, 300);
      setResizable (false);
      setLocationRelativeTo (frame);
      
   }
   
   public JPanel addButtons() {
      
      JPanel pane = new JPanel ();
      
      setLayout (new FlowLayout ());

      
      confirm = new JButton ("Confirm");
      confirm.addActionListener (new RemoveHoldingController (this));
      
      cancel = new JButton ("Cancel");
      cancel.addActionListener (new RemoveHoldingController (this));
      
      pane.add(confirm);
      pane.add(cancel);
      
      return pane;
      
   }
   
   public JPanel itemList() {
      
      JPanel pane = new JPanel();    
      Holding [] holdings = frame.getModel().getAllHoldings();
      String[] list = new String[holdings.length];
      
      for (int i = 0; i < holdings.length; i++) {
         
         list[i] = holdings[i].toString();
         
      }
            
      
      select = new JList<String> (list);
      select.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
      pane.add(select);
      
      return pane;
      
   }
   
   public AppFrame getFrame() {
      
      return this.frame;
      
   }
   
   public JList<String> getJList() {

      return this.select;
      
   }

}
