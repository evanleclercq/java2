package lms.view.dialog;

import java.awt.*;

import javax.swing.*;

import lms.controller.AddCollectionController;
import lms.view.AppFrame;


public class AddCollectionDialog extends JDialog
{

   // dialog box for when the application starts and asks the user to add the collection details
   
   // variables declared for the dialog
   
   private static final long serialVersionUID = 1L; // default added to remove warning

   private AppFrame frame;
   
   private JButton create, cancel;
   private JTextField libCollection, libName;
   private JLabel collection, name;
   
   
   // inital constructor for the dialog box
   
   public AddCollectionDialog (AppFrame frame) {
      
      this.frame = frame;
      this.setTitle ("Add Library Collection Details");
      
      create = new JButton ("Create");
      create.addActionListener(new AddCollectionController (this));
      cancel = new JButton ("Cancel");
      cancel.addActionListener(new AddCollectionController (this));
      
      libCollection = new JTextField (25);
      libName = new JTextField (25);
      
      collection = new JLabel ("Enter Collection Code: ");
      name = new JLabel ("Enter Collection Title: ");
      
      
      this.setLayout (new BorderLayout ());
      
      this.add (addCode(), BorderLayout.NORTH);
      this.add (addTitle(), BorderLayout.CENTER);
      this.add (addButtons(), BorderLayout.SOUTH);
      
      setVisible(true);
      setSize (500, 130);
      setResizable (false);
      setLocationRelativeTo (null);
      
      validate();
      
      
   }
   
   
   // creates the panels to add to the main dialog frame
   
   public JPanel addButtons () {
      
      JPanel panel = new JPanel();
      
      panel.setLayout (new FlowLayout());
      
      panel.add(create);
      panel.add(cancel);      
      
      return panel;
      
   }
   
   public JPanel addTitle () {
      
      JPanel panel = new JPanel();
      
      panel.setLayout (new FlowLayout());
      
      panel.add(name);
      panel.add(libName);      
      
      return panel;
      
   }
   
   public JPanel addCode () {
      
      JPanel panel = new JPanel();
      
      panel.setLayout (new FlowLayout());
      
      panel.add(collection);
      panel.add(libCollection);      
      
      return panel;
      
   }
   
   
   // getters for components of the dialog
   
   public AppFrame getFrame() {
      
      return this.frame;
      
   }
   
   public String getLibCode () {
      
      return libCollection.getText();
      
   }
   
   public String getLibTitle () {
      
      return libName.getText();
      
   }
   
}
