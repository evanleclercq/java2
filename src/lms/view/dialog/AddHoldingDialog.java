package lms.view.dialog;

import java.awt.*;

import javax.swing.*;

import lms.controller.AddHoldingController;
import lms.controller.AddHoldingTypeListener;
import lms.view.AppFrame;
import lms.view.AppToolBar;

public class AddHoldingDialog extends JDialog
{
   
   // creates a dialog box when the user requests to add a holding to the collection
   
   
   // decalred varialbes for the dialog
   
   private static final long serialVersionUID = 1L; // default added to remove warning

   private JPanel panelTop, panelMid, panelBot, dialog;
   
   private AppToolBar panel;
   private AppFrame frame;
   private String holdingType = "book";
   
   private JLabel holdingTitle, holdingCode, holdingFee;
   private JTextField titleInput, codeInput, feeInput;
   private JRadioButton addBook, addVideo;
   
   private JButton create, cancel;
   
   
   // contructor to setup the dialog
      
   public AddHoldingDialog (AppFrame frame) {

      this.frame = frame;
   
      dialog = new JPanel();
      
      this.add(dialog);
     
      dialog.setLayout (new BorderLayout());
      
      holdingCode = new JLabel ("Enter Holding Code: ");
      holdingTitle = new JLabel ("Enter Holding Title: ");
      codeInput = new JTextField (15);
      titleInput = new JTextField (15);
      
      holdingFee = new JLabel ("Enter Holding Fee: ");
      
      panelTop = holdingSelect();
      dialog.add(panelTop, BorderLayout.NORTH);
      
      panelMid = fieldPanel();
      dialog.add(panelMid, BorderLayout.CENTER);
      
      panelBot = addButtons();
      dialog.add(panelBot, BorderLayout.SOUTH);
      
      
      this.setVisible(true);
      this.setLocationRelativeTo(this.panel);
      this.setSize(350, 185);
      this.setResizable(false);
      this.setLocationRelativeTo(panel);
      
      this.validate();
      
   }

   
   // changes the options depending on the holding type being added
   
   public JPanel holdingSelect () {
      
      JPanel pane = new JPanel();
      
      pane.setLayout (new FlowLayout() );
            
      addBook = new JRadioButton("New Book", true);
      addBook.addActionListener (new AddHoldingTypeListener(this));
      addVideo = new JRadioButton("New Video");
      addVideo.addActionListener (new AddHoldingTypeListener(this));
      
      ButtonGroup group = new ButtonGroup ();
      
      group.add(addBook);
      group.add(addVideo);
      
      pane.add(addBook);
      pane.add(addVideo);
      
      return pane;
      
   }
   
   
   // methods to create the panels to be added to the dialog
   
   public JPanel addButtons () {
      
      JPanel pane = new JPanel();
      
      pane.setLayout(new FlowLayout ());
      
      create = new JButton ("Create");
      create.addActionListener (new AddHoldingController(this));
      
      cancel = new JButton ("Cancel");
      cancel.addActionListener (new AddHoldingController(this));
      
      pane.add(create);
      pane.add(cancel);
      
      return pane;
      
   }
   
   public JPanel fieldPanel () {
      
      JPanel pane = new JPanel();
      
      JPanel panel1 = new JPanel();
      JPanel panel2 = new JPanel();
      JPanel panel3 = new JPanel ();
      
      pane.setLayout(new BorderLayout());
      
      panel1.add (holdingTitle);
      panel1.add (titleInput);
      
      panel2.add (holdingCode);
      panel2.add (codeInput);
      
      panel3.add (holdingFee);
      
      if (holdingType.equals("book")) {
                 
         feeInput = new JTextField ("10", 15);
         feeInput.setEditable(false);
         
      }
      
      if (holdingType.equals ("video")) {
         
        feeInput = new JTextField (15);
         
      }
      
      panel3.add (feeInput);
      
      pane.add(panel1, BorderLayout.NORTH);
      pane.add(panel2, BorderLayout.CENTER);
      pane.add(panel3, BorderLayout.SOUTH);
      
      return pane;
      
   }
   
   
   // updates the dialog when the holding type radio buttons are changed
   
   public void updateDialog() {
      
      panelMid.removeAll();
      
      panelMid = fieldPanel();
      dialog.add(panelMid);
      this.validate();
      
   }
   
   
   // getters and setters for the components of the dialog
   
   public String getHoldingType() {
      
      return holdingType;
      
   }
   
   public void setType (String type) {
      
      this.holdingType = type;
      
   }
   
   public AppToolBar getToolBar () {
      
      return this.panel;
      
   }
   
   public String getCode() {
      
      return codeInput.getText();
      
   }

   public String getTitle() {
      
      return titleInput.getText();
      
   }
   
   public int getFee() {
      
      return Integer.parseInt(feeInput.getText());
      
   }
   
   public AppFrame getFrame () {
      
      return this.frame;
      
   }
   
}
