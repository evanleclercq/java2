package lms.view.dialog;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.*;

import lms.controller.ResetController;
import lms.view.AppFrame;

public class ResetDialog extends JDialog
{

   // create dialog box to confirm reset of the entire library collection.
   // is created when the user selects the reset collection option from the menu
   
   
   private static final long serialVersionUID = 1L; // default added to remove warning
   private JButton confirm, cancel;
   private JLabel query;
   private AppFrame frame;
   
   public ResetDialog (AppFrame frame) {
      
      this.frame = frame;
      
      query = new JLabel ("Are you sure you want to Reset?");
      
      setTitle ("Confirm Reset");
      setLayout (new BorderLayout());
      
      add (query, BorderLayout.NORTH);
      add (addButtons(), BorderLayout.CENTER);
      
      
      setVisible(true);
      setSize (250, 100);
      setResizable (false);
      setLocationRelativeTo (frame);
      
//      validate();
      
   }
   
   public JPanel addButtons() {
      
      JPanel panel = new JPanel ();
      
      setLayout (new FlowLayout ());

      
      confirm = new JButton ("Yes");
      confirm.addActionListener (new ResetController (this));
      
      cancel = new JButton ("No");
      cancel.addActionListener (new ResetController (this));
      
      panel.add(confirm);
      panel.add(cancel);
      
      return panel;
      
   }
   
   public AppFrame getFrame () {
      
      return this.frame;
      
   }
   
}
