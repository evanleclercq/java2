package lms.view;

import java.awt.*;
import javax.swing.*;

import lms.controller.SortOrderListener;
import lms.controller.ToolbarController;

public class AppToolBar extends JPanel
{
   
   // Instantiates the objects that will be placed in the AppToolBar panel
   
   private static final long serialVersionUID = 1L; // default added to remove warning
   private JButton button1, button2, button3;
   private AppFrame frame;
   private JRadioButton radio1, radio2, radio3;
   

   // Creates the main panel containing all buttons and radio buttons
   
   public AppToolBar (AppFrame frame) {
      
      this.frame = frame;
      
      setLayout (new BorderLayout());
//      setBackground (Color.LIGHT_GRAY);
      
      add(createButtons(), BorderLayout.WEST);
      add(createSort(), BorderLayout.EAST);
      
      this.setSize (1200, 200);
      
   }
   
  
   // Adds the buttons to the left hand side of the main panel
   
   public JPanel createButtons () {
      
      JPanel panel = new JPanel ();
      
      panel.setLayout (new FlowLayout(FlowLayout.LEFT));
      
      button1 = new JButton ("Reset Library Collection");
      button1.addActionListener (new ToolbarController (this));
      panel.add(button1);
      
      button2 = new JButton ("Add Holding");
      button2.addActionListener (new ToolbarController (this));
      panel.add(button2);
      
      button3 = new JButton ("Remove Holding");
      button3.addActionListener (new ToolbarController (this));
      panel.add(button3);
      
      return panel;
      
   }
   
   
   // Adds the radio buttons to the right hand side of the main panel
   
   public JPanel createSort () {
      
      JPanel panel = new JPanel ();
      
      panel.setLayout(new FlowLayout());
      
      radio1 = new JRadioButton ("NONE", true);
      radio1.addActionListener (new SortOrderListener (frame));
      radio2 = new JRadioButton ("CODE");
      radio2.addActionListener (new SortOrderListener (frame));
      radio3 = new JRadioButton ("TYPE");
      radio3.addActionListener (new SortOrderListener (frame));
      
      ButtonGroup group = new ButtonGroup();
      
      group.add(radio1);
      group.add(radio2);
      group.add(radio3);
      
      panel.add(radio1);
      panel.add(radio2);
      panel.add(radio3);
      
      panel.setBorder (BorderFactory.createTitledBorder("  Sort Options  ")); 
      
      return panel;      
      
   }
   
   public AppFrame getFrame() {
      
      return this.frame;
      
   }
   
}
