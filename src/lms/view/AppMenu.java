package lms.view;

import java.awt.event.KeyEvent;

import javax.swing.*;

import lms.controller.MenuController;

public class AppMenu extends JMenuBar
{

   // delcare components required for the menu
   private static final long serialVersionUID = 1L; // default added to remove warning
   private AppFrame frame;
   private JMenu file, holding;
   private JMenuItem exit, reset, remove, test, addHolding;
   
   public AppMenu (AppFrame frame) {
      
      this.frame = frame;
      
      //Add Menu Options to the Menu Bar
      
      file = new JMenu("File");
      file.setMnemonic (KeyEvent.VK_F);
      add(file);
      holding = new JMenu ("Holdings");
      holding.setMnemonic(KeyEvent.VK_H);
      add(holding);
      
      
      //Adding Menu Items to File option
      
      test = new JMenuItem ("Load Test Utility");
      test.setMnemonic(KeyEvent.VK_T);
      file.add(test);
      test.addActionListener (new MenuController(frame));
      
      reset = new JMenuItem ("Reset Library Collection");
      reset.setMnemonic(KeyEvent.VK_R);
      file.add(reset);
      reset.addActionListener (new MenuController(frame));
      
      exit = new JMenuItem ("Exit");
      exit.setMnemonic(KeyEvent.VK_X);
      file.add(exit);
      exit.addActionListener (new MenuController (frame));
      
      
      //Add Menu Item to Holdings Option
      
      addHolding = new JMenuItem ("Add Holding");
      addHolding.setMnemonic(KeyEvent.VK_A);
      holding.add(addHolding);
      addHolding.addActionListener(new MenuController (frame));
      
      remove = new JMenuItem ("Remove Holding");
      remove.setMnemonic(KeyEvent.VK_V);
      holding.add(remove);
      remove.addActionListener(new MenuController (frame));

      
      
//      setBackground (Color.LIGHT_GRAY);
      
   }

   public AppFrame getFrame() {
      
      return this.frame;
      
   }
   
}
