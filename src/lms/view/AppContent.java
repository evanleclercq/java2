package lms.view;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.Arrays;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

import lms.controller.ContentController;
import lms.controller.HoldingCodeComparator;
import lms.controller.HoldingTypeComparator;
import lms.model.Holding;

public class AppContent extends JPanel
{
   
  
   // declared variables for the panel
   
   private static final long serialVersionUID = 1L; // default added to remove warning
   private int holdingCount = 0;
   private AppFrame frame;
   private String sortOrder = "none";
   private GridLayout layout;
   
   private JLabel holdingCode, holdingTitle, holdingPeriod, holdingFee;
   
   
   // initial setup for the content panel
   
   public AppContent (AppFrame frame) {
      
      setBackground (Color.WHITE); 
      this.frame = frame;
      
      this.setLayout (new FlowLayout());
      this.add(new JLabel ("No Collection Set"));
      this.setAlignmentY(CENTER_ALIGNMENT);
      this.setAlignmentX(CENTER_ALIGNMENT);
      
   }
   
   public void drawEmptyPanel () {
      
      JLabel label = new JLabel ("No Items in Collection");
      
      this.removeAll();
      
      layout = new GridLayout (1, 1, 4, 4);
      
      this.setLayout(layout);
            
      this.add(label);

      label.setHorizontalAlignment(SwingConstants.CENTER);
            
   }
   
   
   // Method to redraw the content panel when collection is updated
   // Includes sorting of the panel
   
   public void redrawPanel () {
      
      this.removeAll();
      
      holdingCount = frame.getModel().countBooks() + frame.getModel().countVideos();
      Holding holdings[] = frame.getModel().getAllHoldings();
      
      
      // changes the sort order of the holdings according to the radio buttons in toolbar
      if (sortOrder.equalsIgnoreCase("type")) {
         
         Arrays.sort(holdings, new HoldingTypeComparator());
                  
      } else if (sortOrder.equalsIgnoreCase("code")) {
         
         Arrays.sort(holdings, new HoldingCodeComparator());
         
      }
      
      for (Holding item : holdings) {
         
         System.out.println (item.toString());
         
      }
      
      System.out.println (sortOrder);
      
      System.out.println (holdingCount);
     
      
      // sets the layout of the content panel according to the number of holdings
      // in the colletion
      if (holdingCount < 4) {
         
//         this.setLayout (new GridLayout (1, holdingCount, 4, 4));
         layout.setColumns(holdingCount);
         layout.setRows(1);
         
         
         for (Holding item : holdings) {
            
            JPanel temp = drawItemPanel(item);
            
            this.add(temp);
            this.add(new JScrollPane(temp));            
            
         }
         
      } else {
         
         int noRows;
         
         if (holdingCount % 4 > 0) {
            
            noRows = (holdingCount / 4) + 1;;
            
         } else {
            
            noRows = (holdingCount / 4);
            
         }
         
         
         
//         this.setLayout (new GridLayout (noRows, 4, 4, 4));
         layout.setColumns(4);
         layout.setRows(noRows);
         
         for (Holding item : holdings) {
            
            JPanel temp = drawItemPanel(item);
            
            this.add(temp);
            this.add(new JScrollPane(temp));            
            
         }
         
         if (holdingCount < (4 * noRows)) {
            
            int diff = (4 * noRows) - holdingCount;
            
            for (int i = 0; i < diff; i++) {
               
               this.add(addEmpty());
               
            }
            
         }
         
      }
      
      this.validate();
      this.repaint();
      
            
   }
   
   
   // method to create each of the holding panels with the required information
   
   public JPanel drawItemPanel (Holding holding) {
      
      String holdingInfo = holding.toString();
      
      String holdingArray[] = holdingInfo.split(":");
      
      JPanel panel = new JPanel ();
      
      panel.setLayout (new GridLayout (4, 1));
      panel.setBackground(Color.WHITE);
      
      holdingCode = new JLabel ("    Holding ID: " + holdingArray[0]);
      holdingTitle = new JLabel ("    Title: " + holdingArray[1]);
      holdingPeriod = new JLabel ("    Loan Period: " + holdingArray [3]);
      holdingFee = new JLabel ("    Default Loan Fee: " + holdingArray [2]);
      
      
      if (holdingArray[4].equals("BOOK")) {
         
         panel.setBorder(BorderFactory.createLineBorder(Color.BLUE, 3));
         
      } else if (holdingArray[4].equals ("VIDEO")) {
         
         panel.setBorder(BorderFactory.createLineBorder(Color.RED, 3));
         
      }
      
      
      panel.add(holdingCode);
      panel.add(holdingTitle);
      panel.add(holdingFee);
      panel.add(holdingPeriod);
      
      panel.setFocusable(true);
      
      panel.addMouseListener (new ContentController (holding, frame));
      
      return panel;
      
   }
   
   public void setSortOrder (String str) {
      
      this.sortOrder = str;
      
   }
   
   
   // creates and empty panel when there are blank spaces in the content pane
   
   public JPanel addEmpty() {
      
      JPanel pane = new JPanel();
      
      pane.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
      pane.setBackground(Color.WHITE);
      
      return pane;
      
   }
   
}
