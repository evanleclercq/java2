package lms.view;

import java.awt.*;
import javax.swing.*;

import lms.controller.FrameController;
import lms.model.facade.LMSFacade;
import lms.view.dialog.AddCollectionDialog;

public class AppFrame extends JFrame
{

   /**
    * 
    */
   private static final long serialVersionUID = 1L; // default added to remove warning

   private boolean collectionSet = false;
   
   // variables for the containers used to setup the GUI
   
   private LMSFacade model;
   private AppToolBar tools;
   private AppContent content;
   private AppStatusBar statusBar;
   private AppMenu menu;
      
   
   // Method to create inital GUI setup to start.
   
   public AppFrame (LMSFacade model) {
      
      tools = new AppToolBar(this);
      content = new AppContent(this);
      statusBar = new AppStatusBar(this);
      menu = new AppMenu (this);
      
      this.model = model;
      
      
      setJMenuBar(menu);
      
      JPanel contentPane = (JPanel) getContentPane ();
      
      contentPane.setLayout(new BorderLayout());

      contentPane.add(tools, BorderLayout.NORTH);
      contentPane.add (content, BorderLayout.CENTER);
      contentPane.add (statusBar, BorderLayout.SOUTH);
      
      this.addWindowListener (new FrameController (this));
      
      setDefaultCloseOperation (JFrame.DO_NOTHING_ON_CLOSE);
      setTitle ("Assignment 2: Library Management System");
      setSize (1200, 800);
      setLocationRelativeTo (null);
      
      validate ();
      setVisible(true);
      
      
      // create dialog box to make the Library Collection
      
      new AddCollectionDialog (this);

   }
   
   
   // Getters and Setters for required components
   
   public LMSFacade getModel () {
      
      return this.model;
      
   }
   
   
   // Method to update the status bar with collection details
   
   public void updateStatusBar () {
      
      statusBar.updateDetails ();
      
   }
   
   
   // Method to redraw the content pane when holdings are added/removed from collection
   
   public void updateContent() {
      
      content.redrawPanel();
      
   }
   
   public void updateContentEmpty () {
      
      content.drawEmptyPanel();
      
   }
   
   public boolean getCollectionStatus () {
      
      return collectionSet;
      
   }
   
   public void setCollectionStatus (boolean state) {
      
      collectionSet = state;
      
   }
   
   public void changeSortOrder (String str) {
      
      content.setSortOrder(str);
      
   }
   
}
