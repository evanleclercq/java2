package lms.view;

import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.*;

public class AppStatusBar extends JPanel
{

   // declare variables for the status bar
   
   private static final long serialVersionUID = 1L; // default added to remove warning
   private JLabel status;
   private AppFrame frame;
   
   
   // initial contructor for the StatusBar
   
   public AppStatusBar (AppFrame frame) {
      
      this.frame = frame;
      status = new JLabel();
      
      this.setLayout(new FlowLayout(FlowLayout.LEFT));
      
//      setBackground (Color.LIGHT_GRAY);
      setOpaque(true);
      setForeground (Color.BLACK);
      
      status.setText ("No Collections in Library");
            
      this.add(status);
      setSize (1200, 50);
      
   }
   
   
   // Method call to update the status bar when collection is created/updated
   
   public void updateDetails () {
      
      String colCode = frame.getModel().getCollection().getCode();
      String colName = frame.getModel().getCollection().getName();
      int noBooks = frame.getModel().countBooks();
      int noVids = frame.getModel().countVideos();
      
      status.setText("  " + colCode + " : " + colName + "    |    Number of Books [" + noBooks + 
                   "]    |    Number of Videos [" + noVids + "]    |");
               
   }
   
}
