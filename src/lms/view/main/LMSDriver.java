package lms.view.main;

import lms.model.facade.*;
import lms.view.*;

public class LMSDriver
{
   
   // Initialize Static variables of main application components
   
   private static AppFrame application;
   private static LMSFacade model = new LMSFacade();
      
   public static void main (String [] args) {
      
      application = new AppFrame (model);
//      application.setVisible(true);
            
   }
  
   
   // Default getters and setters for the two static variables

   public static AppFrame getApplication()
   {
      return application;
   }

   public static LMSModel getModel()
   {
      return model;
   }
   
}

