package lms.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import lms.model.Book;
import lms.model.Video;
import lms.view.dialog.AddHoldingDialog;

public class AddHoldingController implements ActionListener
{

   private AddHoldingDialog dialog;
   
   public AddHoldingController (AddHoldingDialog dialog) {
      
      super();
      this.dialog = dialog;
      
   }
   
   @Override
   public void actionPerformed(ActionEvent e)
   {

      String action = e.getActionCommand();
      
      if (action.equals("Create")) {
         
         try {
            
            Integer.parseInt(dialog.getCode());
            
            if (dialog.getTitle().length() < 3) {
               
               JOptionPane.showMessageDialog (dialog.getFrame(), "Not Enough characters in title", "Error", 
                                              JOptionPane.ERROR_MESSAGE);
               
            } else if (dialog.getCode().length() != 7) {
               
               JOptionPane.showMessageDialog (dialog.getFrame(), "Holding ID must be 7 digits", "Error", 
                                              JOptionPane.ERROR_MESSAGE);

            } else {
            
               if (dialog.getHoldingType().equals ("book")) {
                  
                  int code = Integer.parseInt(dialog.getCode());
                  String title = dialog.getTitle();
      //            int fee = dialog.getFee();
                  
                  dialog.getFrame().getModel().addHolding(new Book(code, title));
                  
               } else if (dialog.getHoldingType().equals ("video")) {
                  
                  int code = Integer.parseInt(dialog.getCode());
                  String title = dialog.getTitle();
                  int fee = dialog.getFee();
                  
                  if (fee != 4 || fee != 6) {
                     
                     JOptionPane.showMessageDialog (dialog.getFrame(), "Video Fee must be 4 or 6", "Error", 
                                                    JOptionPane.ERROR_MESSAGE);
                     
                  } else {
                  
                     dialog.getFrame().getModel().addHolding(new Video(code, title, fee));
                  
                  }
                     
               }
               
               dialog.getFrame().updateStatusBar();
               dialog.getFrame().updateContent();
               
               dialog.setVisible(false);
               dialog.dispose();
               
            }
         
         } catch (NumberFormatException ex) {
            
            JOptionPane.showMessageDialog (dialog.getFrame(), "Holding ID not numberical", "Error", 
                                           JOptionPane.ERROR_MESSAGE);
         }
         

         
         
      } else if (action.equals ("Cancel")) {
         
         dialog.setVisible(false);
         dialog.dispose();
         
      }      
      
   }

}
