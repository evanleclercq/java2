package lms.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import lms.view.dialog.CloseDialog;


public class CloseController implements ActionListener
{

   // controller for the close dialog box/
   // will either exit application or close dialog
   
   private CloseDialog dialog;
   
   public CloseController (CloseDialog dialog) {
      
      super ();
      this.dialog = dialog;
      
   }

   @Override
   public void actionPerformed(ActionEvent e)
   {
      
      String action = e.getActionCommand();
      
      if (action.equals ("Yes")) {
         
         System.out.println("Exit");
         dialog.getFrame().setVisible(false);
         dialog.getFrame().dispose();
         System.exit(0);
        
      } else if (action.equals ("No")) {
         
         dialog.setVisible(false);
         dialog.dispose();
         
      }
      

   }

}
