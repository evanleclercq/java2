package lms.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import lms.view.AppFrame;

public class SortOrderListener implements ActionListener
{

   // listner to monitor the sort type radio buttons on the tool bar
   // and allow for the panel to be repainted with new order
   
   private AppFrame frame;
   
   public SortOrderListener (AppFrame frame) {
      
      super();
      this.frame = frame;
      
   }
   
   @Override
   public void actionPerformed(ActionEvent e)
   {

      String action = e.getActionCommand();
      
      if (action.equalsIgnoreCase("none")) {
         
         frame.changeSortOrder("none");
         frame.updateContent();
         
      }
      
      if (action.equalsIgnoreCase ("type")) {
         
         frame.changeSortOrder("type");
         frame.updateContent();
         
      }
      
      if (action.equalsIgnoreCase("code")) {
         
         frame.changeSortOrder("code");
         frame.updateContent();
         
      }    

   }

}
