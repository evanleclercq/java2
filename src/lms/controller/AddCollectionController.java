package lms.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import lms.model.LibraryCollection;
import lms.view.dialog.AddCollectionDialog;

public class AddCollectionController implements ActionListener
{
   
   private AddCollectionDialog dialog;
   
   public AddCollectionController (AddCollectionDialog dialog) {
      
      super ();
      this.dialog = dialog;
      
   }

   @Override
   public void actionPerformed(ActionEvent e)
   {
      
      // Confirms the button action pressed
      
      String action = e.getActionCommand();
      
      // creates the new collection and updates the required panels
      // updates the content panel and the status bar.
      
      if (action.equals ("Create")) {
         
         String code = dialog.getLibCode();
         String name = dialog.getLibTitle();
         
         System.out.println ("New Library Collection Created"); //console print used while testing
         
         dialog.getFrame().getModel().addCollection(new LibraryCollection (code, name));
         dialog.getFrame().updateStatusBar();
         dialog.getFrame().setCollectionStatus (true);
         dialog.getFrame().updateContentEmpty();
         
         // closes the dialog box once everything has been updated
         dialog.setVisible(false);
         dialog.dispose();

         
      }
      
      
      // closed the dialog box and stops the application if no collection is created.
      if (action.equals ("Cancel")) {
         
         System.out.println("Exit");
         dialog.getFrame().setVisible(false);
         dialog.getFrame().dispose();
         System.exit(0);
         
      }

   }

}
