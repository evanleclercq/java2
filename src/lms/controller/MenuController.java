package lms.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import lms.view.AppFrame;
import lms.view.dialog.AddHoldingDialog;
import lms.view.dialog.CloseDialog;
import lms.view.dialog.RemoveHoldingDialog;
import lms.view.dialog.ResetDialog;
import lms.view.util.Tester;

public class MenuController implements ActionListener
{
   
   // listener to control the menu bar and action the selection made

   private AppFrame frame;
   
   public MenuController (AppFrame frame) {
      
      super ();
      this.frame = frame;
      
   }
   
   @Override
   public void actionPerformed(ActionEvent e)
   {

      String action = e.getActionCommand();
      
      if (action.equals ("Exit")) {
         
         new CloseDialog(frame);
         
      }
      
      if (action.equals ("Add Holding")) {
         
         new AddHoldingDialog (frame);
         
      }
      
      if (action.equals ("Remove Holdings")) {
         
         new RemoveHoldingDialog (frame);
         
      }
      
      if (action.equals ("Load Test Utility")) {
         
         Tester data = new Tester();
         data.setupTestData(frame.getModel());
         frame.updateStatusBar();
         frame.updateContent();
         
      }
      
      if (action.equals("Reset Library Collection")) {
         
         new ResetDialog(frame);
         
      }

   }

}
