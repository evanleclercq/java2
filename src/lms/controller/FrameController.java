package lms.controller;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import lms.view.AppFrame;
import lms.view.dialog.CloseDialog;

public class FrameController implements WindowListener
{
   
   // listener to create the close dialog box when the frame close
   // button is pressed
   
   AppFrame frame;
   
   public FrameController (AppFrame frame) {
      
      super();
      this.frame = frame;
      
   }

   @Override
   public void windowActivated(WindowEvent arg0)
   {
      // TODO Auto-generated method stub

   }

   @Override
   public void windowClosed(WindowEvent arg0)
   {

      // TODO Auto-generated method stub      

   }

   @Override
   public void windowClosing(WindowEvent arg0)
   {

      new CloseDialog (frame);

   }

   @Override
   public void windowDeactivated(WindowEvent arg0)
   {
      // TODO Auto-generated method stub

   }

   @Override
   public void windowDeiconified(WindowEvent arg0)
   {
      // TODO Auto-generated method stub

   }

   @Override
   public void windowIconified(WindowEvent arg0)
   {
      // TODO Auto-generated method stub

   }

   @Override
   public void windowOpened(WindowEvent arg0)
   {
      // TODO Auto-generated method stub

   }

}
