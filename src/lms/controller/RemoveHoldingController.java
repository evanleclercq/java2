package lms.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import lms.view.dialog.RemoveHoldingDialog;

public class RemoveHoldingController implements ActionListener
{
   
   // listener to remove multiple holdings when the user selects the
   // remove holdings function

   private RemoveHoldingDialog dialog;
   private int[] temp;
   
   public RemoveHoldingController (RemoveHoldingDialog dialog) {
      
      super();
      this.dialog = dialog;
      
   }
   
   @Override
   public void actionPerformed(ActionEvent e)
   {
      
      String action = e.getActionCommand();
          
      if (action.equals("Confirm")) {
         
         List<String> str = dialog.getJList().getSelectedValuesList();
         
         
         temp = dialog.getJList().getSelectedIndices();
         
         for (int i : temp) {
            
            System.out.println(i);
            
         }
         
         for (String list : str) {
            
            String[] details =  (list).split (":");
            
            dialog.getFrame().getModel().removeHolding(Integer.parseInt(details[0]));
            
         }
         
         dialog.getFrame().updateContent();
         dialog.getFrame().updateStatusBar();
         
         dialog.setVisible(false);
         dialog.dispose();         
         
      } else if (action.equals ("Cancel")) {
         
         dialog.setVisible(false);
         dialog.dispose();
         
      }     

   }

}
