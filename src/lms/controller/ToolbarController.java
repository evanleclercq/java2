package lms.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import lms.view.AppToolBar;
import lms.view.dialog.AddHoldingDialog;
import lms.view.dialog.RemoveHoldingDialog;
import lms.view.dialog.ResetDialog;

public class ToolbarController implements ActionListener
{
   
   // listener to activate the functions from the tool bar
   // and create the relative dialog box.

   private AppToolBar panel;
   
   
   public ToolbarController (AppToolBar panel) {
      
      super();
      this.panel = panel;
      
   }
   
   
   @Override
   public void actionPerformed(ActionEvent e)
   {

      String action = e.getActionCommand();
      
      if (action.equals ("Reset Library Collection")) {
         
         new ResetDialog (panel.getFrame());
         
      }
      
      if (action.equals ("Add Holding")) {
         
         new AddHoldingDialog (panel.getFrame());
         
      }
      
      if (action.equals ("Remove Holding")) {
         
         new RemoveHoldingDialog (panel.getFrame());
         
      }
      
   }

}
