package lms.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import lms.model.Holding;
import lms.model.facade.LMSFacade;
import lms.view.dialog.ResetDialog;

public class ResetController implements ActionListener
{
   
   // listner to confirm reset of the library collecton

   ResetDialog dialog;
   
   public ResetController (ResetDialog dialog) {
      
      super();
      this.dialog = dialog;
      
   }
   
   @Override
   public void actionPerformed(ActionEvent e)
   {

      String action = e.getActionCommand();
      
      if (action.equals ("Yes")) {
         
         LMSFacade model = dialog.getFrame().getModel();
         Holding[] holdings = model.getAllHoldings();
         
         for (Holding item : holdings) {
            
            model.removeHolding(item.getCode());
                        
         }
         
         dialog.getFrame().updateStatusBar();
         dialog.getFrame().updateContentEmpty();
         dialog.setVisible(false);
         dialog.dispose();
         
      }
      
      if (action.equals ("No")) {
         
         dialog.setVisible(false);
         dialog.dispose();
         
      }

   }

}
