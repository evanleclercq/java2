package lms.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import lms.model.Holding;
import lms.view.dialog.RemoveSingleDialog;

public class RemoveSingleController implements ActionListener
{
   
   // listener of the single dialog removal dialog to confirm the removal

   private RemoveSingleDialog dialog;
   private Holding holding;
   
   public RemoveSingleController (RemoveSingleDialog dialog, Holding holding) {
      
      super();
      this.dialog = dialog;
      this.holding = holding;
      
   }
   
   @Override
   public void actionPerformed(ActionEvent e)
   {

      String action = e.getActionCommand();
      
      if (action.equals("Confirm")) {
         
         dialog.getFrame().getModel().removeHolding(holding.getCode());
         dialog.getFrame().updateStatusBar();
         
         if (dialog.getFrame().getModel().countBooks() == 0) {
            if (dialog.getFrame().getModel().countVideos() == 0) {
               dialog.getFrame().updateContentEmpty();
            }
         } else {
         
         dialog.getFrame().updateContent();
         
         }
         
         dialog.setVisible(false);
         dialog.dispose();
         
      }
      
      if (action.equals("Cancel")) {
         
         dialog.setVisible(false);
         dialog.dispose();
         
      }
      
   }

}
