package lms.controller;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import lms.model.Holding;
import lms.view.AppFrame;
import lms.view.dialog.RemoveSingleDialog;

// mouse listener to watch for the double-click to remove holding function

public class ContentController implements MouseListener
{
   
   private Holding holding;
   private AppFrame frame;

   public ContentController (Holding holding, AppFrame frame) {
      
      super();
      this.holding = holding;
      this.frame = frame;
      
   }

   @Override
   public void mouseClicked(MouseEvent e)
   {
      
      if (e.getClickCount() == 2) {
         
         System.out.println ("Double Clicked: " + holding.toString());
         new RemoveSingleDialog(holding, frame);
         
      }
      
   }

   @Override
   public void mouseEntered(MouseEvent arg0)
   {
      // TODO Auto-generated method stub
      
   }

   @Override
   public void mouseExited(MouseEvent arg0)
   {
      // TODO Auto-generated method stub
      
   }

   @Override
   public void mousePressed(MouseEvent arg0)
   {
      // TODO Auto-generated method stub
      
   }

   @Override
   public void mouseReleased(MouseEvent arg0)
   {
      // TODO Auto-generated method stub
      
   }

}
