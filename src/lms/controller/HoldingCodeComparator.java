package lms.controller;

import java.util.Comparator;

import lms.model.Holding;

public class HoldingCodeComparator implements Comparator<Holding>
{

   // compartor to sort the holdings in the content panel according
   // to the holding codes in the collection
   
   @Override
   public int compare(Holding o1, Holding o2)
   {
      
      return Integer.valueOf(o1.getCode())
               .compareTo(o2.getCode());
      
   }   
   
}