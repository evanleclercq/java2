package lms.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import lms.view.dialog.AddHoldingDialog;

public class AddHoldingTypeListener implements ActionListener
{
   
   // Listener to change the options fo the add holding dialog box
   // depending on what holding type is selected in the radio buttons.
   
   AddHoldingDialog dialog;
   
   public AddHoldingTypeListener (AddHoldingDialog dialog) {
      
      super();
      this.dialog = dialog;
      
   }

   @Override
   public void actionPerformed(ActionEvent e)
   {
      
      String action = e.getActionCommand();
      
      if (action.equals("New Book")) {
         
         dialog.setType("book");
         System.out.println(action + " | " + dialog.getHoldingType());
         dialog.updateDialog();
         
      } else if (action.equals ("New Video")) {
         
         dialog.setType("video");
         System.out.println(action + " | " + dialog.getHoldingType());
       dialog.updateDialog();

         
      }
      
   }

}
