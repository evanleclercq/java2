package lms.controller;

import java.util.Comparator;

import lms.model.Holding;

public class HoldingTypeComparator implements Comparator<Holding>
{

   // comparator to sort the holdings in the content panel according
   // to the type of holdings in the collection.
   // will display books first and then videos
   
   @Override
   public int compare(Holding o1, Holding o2)
   {

      int result = 0;
      
      if (o1 instanceof lms.model.Book) {
         
         result = 0;
         
      } else {
         
         result = 1;
         
      }
      
      return result;

   }

}
