package lms.model;

import java.util.*;

import lms.model.exception.*;

public class Library
{
   
   private Member member;
   public LibraryCollection collection;
   
//   for simplicity I have just used a single Member object but to easily expand
//   the functionality you could add them into a list as per next line.
//   private List<Member> LibMember = new ArrayList<Member>();

   public void addCollection (LibraryCollection collection) {
      
      this.collection = collection;
      
   }
   
   public boolean addHolding (Holding holding) {
      
      return collection.addHolding (holding);
      
   }
   
   public void addMember (Member addMember) {
            
      this.member = addMember;      
//      LibMember.add(member);
      
   }
   
   public void borrowHolding (int code) 
            throws MultipleBorrowingException, InsufficientCreditException {
      
      boolean error = false;
      HistoryRecord Hx[] = member.getBorrowingHistory();
      
      if (Hx != null) {
      
         for (HistoryRecord item : Hx) {
            
//            System.out.println (item.toString());
            
            if (item.getCode() == code) {
               
               error = true;
               throw new MultipleBorrowingException();
               
            }
            
         }
      
      }
      
      int remainingCredit = member.getCurrentCredit();
      remainingCredit = calculateRemainingCredit();
      int loanFee = getHolding(code).getDefaultLoanFee();
  
//      System.out.println (remainingCredit);
//      System.out.println(loanFee);
      
      if (remainingCredit - loanFee < 0) {
         
         error = true;
         throw new InsufficientCreditException ();
         
      }
      
      if (!error) {
         member.borrowHolding(code);
         collection.getHolding(code).setOnLoan(true);
         collection.getHolding(code).setBorrowDate();
         calculateRemainingCredit();        
         
      }
      
   }
   
   public int calculateRemainingCredit () {
      
//      member.resetCredit();
//      
//      int currentCredit = member.getCurrentCredit();
//      Integer currentHoldings[] = member.getCurrentHoldings();
//      int totalFees = 0;
//      
//      for (int code : currentHoldings) {
//         
//         totalFees = totalFees + collection.getHolding(code).getDefaultLoanFee();
//         System.out.println (totalFees);
//
//      }
//
//      HistoryRecord history[] = getBorrowingHistory();
//      
//      
//      if (history != null) {
//         for (HistoryRecord item : history) {
//            
//            totalFees += item.getFeePayed();
//            
//         }
//      
//      }
//      
//      currentCredit = currentCredit - totalFees;
//      member.setCurrentCredit(currentCredit);
//
//      return currentCredit;
      
//      member.softReset();
      int currentCredit = member.getCurrentCredit ();
      Integer currentHoldings[] = member.getCurrentHoldings ();
      HistoryRecord history[] = member.getBorrowingHistory();
      int currentFees = 0;
      int historyFees = 0;
      
      if (currentHoldings != null) {
         for (int code : currentHoldings) {
            
            if (getHolding(code).chargedFee() == false) {
            
               currentFees = currentFees + getHolding(code).getDefaultLoanFee();
               getHolding(code).setChargedFee(true);
            
            }               
         }
      }
      
      if (history != null) {
         
         for (HistoryRecord item : history) {
            
            if (item.getPayed() == false) {

               historyFees = historyFees + item.getFeePayed();
               item.setPayed(true);
               
            }
            
         }
         
      }
      
      int totalFees = currentFees + historyFees;
      currentCredit = currentCredit - totalFees;
      member.setCurrentCredit (currentCredit);
      
      System.out.printf ("\nCurrent: %d | History: %d | Total: %d\n", 
                         currentFees, historyFees, totalFees);
      System.out.println (currentCredit);
      
      
      return currentCredit;
      
      
   }
   
   public int calculateTotalLateFees () {
      
      return member.getHistory().calculateTotalLateFees();
            
   }
      
   public Holding[] getAllHoldings () {
      
      return collection.getAllHoldings();
      
   }
   
   public Holding[] getBorrowedHoldings () {
      
      List<Holding> borrowed = new ArrayList<Holding>();
      
      for (Holding item : collection.getAllHoldings()) {
         
         if (item.isOnLoan() == true) {
            
            borrowed.add(item);
            
         }
         
      }
      
      Holding[] borrowedHoldings = new Holding[borrowed.size()];
      borrowedHoldings = borrowed.toArray(borrowedHoldings);
      
      if (borrowedHoldings.length == 0) {
         
         borrowedHoldings = null;
         
      }

      return borrowedHoldings;
   
      
   }
   
   public HistoryRecord[] getBorrowingHistory () {
      
           
      if (member.getBorrowingHistory() != null) {
         HistoryRecord rx[] = member.getBorrowingHistory();
         String hx[] = new String[rx.length];
      
         for (int i = 0; i < rx.length; i++) {
            
            hx[i] = rx[1].toString();
            
         }
         
         return rx;
      
      } else {
         
         return null;
         
      }
      
   }
   
   public LibraryCollection getCollection () {
      
      return collection;
      
   }
   
   public HistoryRecord getHistoryRecord (int code) {
      
      HistoryRecord record = null;
      
      HistoryRecord history[] = member.getBorrowingHistory();
      
      for (HistoryRecord item : history) {
         
         if (item.getCode() == code) {
            
            record = item;
            break;
            
         }
         
      }
      
      return record;
      
   }
   
   public Holding getHolding (int code) {
      
      return collection.getHolding(code);
      
   }
   
   public Member getMemeber () {
      
      return member;
      
   }
   
   public boolean removeHolding (int code) {
      return collection.removeHolding(code);
   }
   
   public void resetCredit () {
      
      member.resetCredit ();
      
   }
   
   public boolean returnHolding (int code) 
            throws OverdrawnCreditException {
      
      int currentCredit = calculateRemainingCredit();
      Holding borrowedHoldings[] = getBorrowedHoldings();
      boolean holdingOnLoan = false;
      Holding toReturn = null;
      int lateFee = 0;
      
      boolean bool = false;
      
      if (borrowedHoldings.length == 0) {
         
         System.out.println ("There are currently no holdings on loan");         
         bool =  false;
         
      }
      
//      System.out.println("TEST");
      
      for (Holding item : borrowedHoldings) {
         
         if (item.getCode() == code) {

            holdingOnLoan = true;
            item.setReturnDate();
            toReturn = item;
            
         }
                  
      }
      
      lateFee = toReturn.calculateLateFee();

//      System.out.println("TEST");
      
      if (!holdingOnLoan) {
         
         System.out.printf ("\nHolding %d is not currently on loan\n", code);
         bool = false;
      
      }
      
//      System.out.println("TEST");
//      System.out.println(member.getClass().getName());
//      System.out.println(currentCredit);
//      System.out.println(lateFee);
      
      if (currentCredit - lateFee < 0 && 
               member.getClass().getName() == "lms.model.StandardMember") {
         
//         System.out.println("TEST");
         throw new OverdrawnCreditException ();
                        
      } else {
         
//         System.out.println("TEST");
            
         toReturn.setOnLoan(false);
         bool = member.returnHolding(toReturn, lateFee);
                  
         } 
         
      return bool;
      
   }
      
 

   
   public int countBooks () {
      
      return collection.countBooks();
      
   }
   
   public int countVideos () {
      
      return collection.countVideos();      
   }
   
}
