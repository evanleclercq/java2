package lms.model;

import lms.model.facade.LMSFacade;

public class Book extends AbstractHolding
{

   private final int MAX_FEE = 10;
   private final int MAX_LENGTH = 28;
   private final String TYPE = "BOOK";
   
   public Book(int code, String title)
   {
      super(code, title);
   }
   
   public int getDefaultLoanFee () {
      
      return MAX_FEE;
      
   }
   
   public int getMaxLoanPeriod () {
      
      return MAX_LENGTH;
      
   }

   public int calculateLateFee () {
      
      int fee = 0;
      
      int elapsedDays = LMSFacade.getDate().getElapsedDays(borrowDate);
      
      if (elapsedDays > MAX_LENGTH) {
         
         fee = ((elapsedDays - MAX_LENGTH) * 2);
         
      }
      
      return fee;
      
   }
   
   public String toString () {
      
      String output = super.toString() + ":" + MAX_FEE +
               ":" + MAX_LENGTH + ":" + TYPE;
      return output;
      
   }
   
}
