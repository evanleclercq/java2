package lms.model;

public class HistoryRecord
{

   private Holding holding;
   private int fee;
   private boolean payed = false;
   
   public HistoryRecord (Holding holding, int fee) {
      
      this.holding = holding;
      this.fee = fee;
      
   }
   
   public Holding getHolding()
   {
      return holding;
   }

   
   public int getCode() {
      
      return holding.getCode();
      
   }
   
   
   public int getFeePayed() {
      
      return (fee);
      
   }
   
   public int getTotalFee () {
      
      return (fee + holding.getDefaultLoanFee());
      
   }
   
   
   public String toString () {
      
      String output = holding.getCode() + ":" + (holding.getDefaultLoanFee() +  fee);
      
      return output;
      
   }
   
   public boolean getPayed () {
      
      return payed;
      
   }
   
   public void setPayed (boolean state) {
      
      this.payed = state;
      
   }
   
}
