package lms.model;

import java.util.*;
import lms.model.BorrowingHistory;

public abstract class AbstractMember implements Member
{

   private String id;
   private String name;
   List<Integer> currentHoldings = new ArrayList<Integer>();
   BorrowingHistory borrowingHistory = new BorrowingHistory();

   public AbstractMember (String id, String name) {
      
      this.id = id;
      this.name = name;
      
   }
   
   public void borrowHolding(int code) {
      
      currentHoldings.add(code);
      
   }
   
   public Integer[] getCurrentHoldings () {
      
      Integer holdings[] = new Integer[currentHoldings.size()];
      
      currentHoldings.toArray(holdings);
      
      return holdings;
      
   }
   
   public HistoryRecord[] getBorrowingHistory () {
      
      if (borrowingHistory.getAllHistoryRecords() == null) {
         
         return null;
         
      } else {
      
         return borrowingHistory.getAllHistoryRecords();
         
      }
      
   }
   
   public boolean returnHolding(Holding holding, int fee) {
      
      boolean success = false;
      
      for (int i = 0; i < currentHoldings.size(); i++) {
         
         if (currentHoldings.get(i) == holding.getCode()) {
            currentHoldings.remove(i);
            success = true;
            borrowingHistory.addHistoryRecord(holding, fee);
         }
         
      }
      
      return success;
      
   }
   
   public BorrowingHistory getHistory () {
      
      return borrowingHistory;
      
   }
   
   public String toString () {
      
      String output = id + ":" + name;
      return output;
      
   }
   
   

}


