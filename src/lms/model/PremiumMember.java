package lms.model;

public class PremiumMember extends AbstractMember
{

   private final int MAX_CREDIT = 45;
   private final String TYPE = "PREMIUM";
   private int currentCredit;
   
   public PremiumMember(String id, String name)
   {
      super (id, name);
      currentCredit = MAX_CREDIT;
   }
   
   public String toString() {
      
      String output = super.toString() + ":" + MAX_CREDIT + ":" + TYPE;
      
      return output;
      
   }
   
   public int getCurrentCredit() {

      return currentCredit;
      
   }
   
   public void setCurrentCredit (int amount) {
      
      this.currentCredit = amount;
      
   }
   
   public void resetCredit () {

      currentCredit = MAX_CREDIT;
      
   }
   
   public int getMaxCredit () {
      
      return MAX_CREDIT;
      
   }

}
