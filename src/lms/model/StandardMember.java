package lms.model;

public class StandardMember extends AbstractMember
{

   private final int MAX_CREDIT = 30;
   private final String TYPE = "STANDARD";
   private int currentCredit; 
   
   public StandardMember(String id, String name)
   {
      super(id, name);
      currentCredit = MAX_CREDIT;
   }
   
   
   public String toString() {
      
      String output = super.toString() + ":" + MAX_CREDIT + ":" + TYPE;
      
      return output;
      
   }
   
   public int getCurrentCredit() {

      return currentCredit;
      
   }
   
   public void setCurrentCredit (int amount) {
      
      this.currentCredit = amount;
      
   }
   
   public void resetCredit () {
      
//      super.borrowingHistory.clearRecord();
      
      currentCredit = MAX_CREDIT;
      
   }

   
   public int getMaxCredit () {
      
      return MAX_CREDIT;
      
   }

}
