package lms.model;

public interface Holding
{
   
   public int getCode();
   public int getDefaultLoanFee();
   public int getMaxLoanPeriod();
   public boolean isOnLoan();
   public void setOnLoan(boolean state);
   public void setBorrowDate ();
   public void setReturnDate ();
   public int calculateLateFee ();
   public String getTitle ();
   public String toString();
   public boolean chargedFee ();
   public void setChargedFee (boolean state);
   

}
