package lms.model;

public interface Member
{

   public void borrowHolding(int code);
   public Integer[] getCurrentHoldings();
   public String toString ();
   public int getCurrentCredit();
   public void setCurrentCredit(int amount);
   public void resetCredit ();
   public boolean returnHolding (Holding holding, int lateFee);
   public BorrowingHistory getHistory ();
   public HistoryRecord[] getBorrowingHistory();
   public int getMaxCredit();
   
}
