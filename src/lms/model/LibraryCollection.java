package lms.model;

import java.util.*;

public class LibraryCollection
{
   
   private String code;
   private String name;
   List<Holding> holdings = new ArrayList<Holding>();

   public LibraryCollection(String code, String name)
   {

      this.code = code;
      this.name = name;
      
   }
   
   
   public boolean addHolding (Holding holding) {
      
      if (checkDuplicates(holding)) {
         
         System.out.printf("\nHolding %s already exists\n", holding.toString());
         return false;
         
      } else {
      
         holdings.add(holding);
         return true;
         
      }
      
   }
   
   
   public Holding[] getAllHoldings () {
      
      if (holdings.isEmpty()) {
         
         return null;
         
      } else {
      
      Holding allHoldings[] = new Holding[holdings.size()];
      holdings.toArray(allHoldings);
      
      return allHoldings;
      
      }
      
   }
   
   
   public Holding getHolding (int code) {
      
      Holding found = null;
      
      for (int i = 0; i < holdings.size(); i++) {
         
         if (code == holdings.get(i).getCode()) {
            found = holdings.get(i);
            break;
         }
         
      }
      
      if (found == null) {
         System.out.printf("\nNo holding with code %d was found\n", code);
      }
      
      return found;
      
   }
   
   public boolean removeHolding (int code) {
      
      Holding item = getHolding(code);
      boolean removed = false;
      
      if (item == null) {
         System.out.printf("Holding %d does not exist", code);
         removed = true;
      } else if (item.isOnLoan() == true) {
         System.out.printf("\nHolding %d is currently on loan " + 
                            "and cannot be removed\n", code);
         removed = false;
      } else {
         
         for (int i = 0; i < holdings.size(); i++) {
            
            if (code == holdings.get(i).getCode()) {
               
               holdings.remove(i);
               removed =  true;
            }
            
         }
         
      }
      
      return removed;
      
      
   }
   
   public int countBooks(){
      
      int count = 0;
      
      for (int i = 0; i < holdings.size(); i++) {
         
         if (holdings.get(i).getClass().getName() == "lms.model.Book") {
            count++;
         }
         
      }
      
      return count;
      
   }
   
public int countVideos(){
      
      int count = 0;
      
      for (int i = 0; i < holdings.size(); i++) {
         
         if (holdings.get(i).getClass().getName() == "lms.model.Video") {
            count++;
         }
         
      }
      
      return count;

}

   public String toString () {
      
      String output;
      
      if (holdings.isEmpty()) {
         
         output = code + ":" + name;
         
      } else {
         
         String codeList = "";
         
         for (Holding item : holdings) {
            
            codeList = codeList + "," + item.getCode();
            
         }
     
         output = code + ":" + name + ":" + codeList.substring(1);
         
      }
      
      return output;
      
   }
   
   
   public boolean checkDuplicates (Holding holding) {
      
      boolean duplicate = false;
      
      for (Holding item : holdings) {
         
         if (item.getCode() == holding.getCode()) {
            duplicate = true;
         } else if (item.getTitle() == holding.getTitle()) {
            duplicate = true;
         }
         
      }
      
      return duplicate;
      
   }
   
   
   public String getCode () {
      
      return this.code;
      
   }
   
   public String getName () {
      
      return this.name;
      
   }
   
}
