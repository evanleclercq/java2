package lms.model;

import lms.model.facade.LMSFacade;

public class Video extends AbstractHolding
{

   private final int MAX_FEE;
   private final int MAX_LENGTH = 7;
   private final String TYPE = "VIDEO";
   
   
   
   public Video(int code, String title, int fee)
   {

      super (code, title);
      this.MAX_FEE = fee;
      
   }
   
   public int getDefaultLoanFee () {
      
      return MAX_FEE;
      
   }
   
   public int getMaxLoanPeriod () {
      
      return MAX_LENGTH;
      
   }
   
   public int calculateLateFee () {
      
      int fee = 0;
      
      int elapsedDays = LMSFacade.getDate().getElapsedDays(borrowDate);
      
      if (elapsedDays > MAX_LENGTH) {
         
         fee = (elapsedDays - MAX_LENGTH) * (MAX_FEE / 2);
         
      }
      
      return fee;
      
   }

   
   public String toString () {
      
      String output = super.toString() + ":" + MAX_FEE +
               ":" + MAX_LENGTH + ":" + TYPE;
      return output;
      
   }

}
