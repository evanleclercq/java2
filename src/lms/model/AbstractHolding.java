package lms.model;

import lms.model.facade.*;


public abstract class AbstractHolding implements Holding
{

   private int code;
   private String title;
   private boolean onLoan = false;
   String borrowDate;
   String returnDate;
   boolean chargedFee = false;
      
   public AbstractHolding (int code, String title) {
      
      this.code = code;
      this.title = title;
      
   }
   
   public int getCode() {
      
      return this.code;
      
   }
   
   public boolean isOnLoan() {
      
      return onLoan;
      
   }
   
   public void setOnLoan(boolean state) {
      
      this.onLoan = state;
      
   }
   
   public void setBorrowDate () {
      
      borrowDate = LMSFacade.getDate().getDate();
      
   }
   
   public void setReturnDate () {
      
      returnDate = LMSFacade.getDate().getDate();
      
   }
   
   public String getTitle () {
      
      return title;
      
   }
   
   public String toString () {
      
      String output = code + ":" + title;
      return output;
      
   }
   
   public boolean chargedFee () {
      
      return chargedFee;
      
   }
   
   public void setChargedFee (boolean state) {
      
      chargedFee = state;
      
   }

}
