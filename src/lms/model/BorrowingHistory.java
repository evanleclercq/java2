package lms.model;

import java.util.*;


public class BorrowingHistory
{
   
   private List<HistoryRecord> record = new ArrayList<HistoryRecord>();
   private List<HistoryRecord> archive = new ArrayList<HistoryRecord>();
   
   
   public BorrowingHistory () {}
   
   
   public void addHistoryRecord (Holding holding, int fee) {
      
      record.add(new HistoryRecord(holding, fee));
      archive.add(new HistoryRecord(holding, fee));
      
   }
   
   
   public HistoryRecord[] getAllHistoryRecords () {
            
      HistoryRecord history[] = new HistoryRecord[record.size()];
      record.toArray(history);
      
//      System.out.println (history.length);
      
      if (history.length == 0) {
         
         history = null;
         
      }
      
//      System.out.println(history);
      
      return history;
      
   }
   
   
   public HistoryRecord getHistoryRecord (int code) {
      
      HistoryRecord found = null;
      
      for (HistoryRecord item : record) {
         
         if (item.getCode() == code) {
            
            found = item;
            break;
            
         }
         
      }
      
      if (found == null) {
         
         System.out.printf("No record for item %d found", code);
         
      }
      
      return found;
      
   }
   
   public int calculateTotalLateFees() {
      
      int fees = 0;
      
      for (HistoryRecord item : record) {
         
//         System.out.println (item.getFeePayed());
         fees = fees + item.getFeePayed();
//         System.out.println(fees);
         
      }
      
//      System.out.println(fees);
      return fees;
      
   }
   
  
   public void clearRecord () {
      
      record.clear();
      
   }
   
   
}
