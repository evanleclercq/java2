package lms.model.facade;

import lms.model.HistoryRecord;
import lms.model.Holding;
import lms.model.LibraryCollection;
import lms.model.Member;
import lms.model.exception.InsufficientCreditException;
import lms.model.exception.MultipleBorrowingException;
import lms.model.exception.OverdrawnCreditException;
import lms.model.util.DateUtil;
import lms.model.Library;

public class LMSFacade implements LMSModel {
   
   Library library = new Library();
   static DateUtil date;

   public void setDate (String inputDate) {
      
      date = DateUtil.getInstance();
      
      date.setDate(inputDate);
     
//      System.out.println (date);
      
   }
   
   public static DateUtil getDate() {
      
      return date;
      
   }

   @Override
   public void addMember(Member member)
   {

      library.addMember(member);
      
   }

   @Override
   public void addCollection(LibraryCollection collection)
   {
      library.addCollection(collection);
      
   }

   @Override
   public Member getMember()
   {
      return library.getMemeber();
   }

   @Override
   public LibraryCollection getCollection()
   {
      return library.getCollection();
   }

   @Override
   public boolean addHolding(Holding holding)
   {
      library.addHolding(holding);
      return false;
   }

   @Override
   public boolean removeHolding(int holdingId)
   {
      return library.removeHolding(holdingId);
   }

   @Override
   public Holding getHolding(int holdingId)
   {
      return library.getHolding(holdingId);
   }

   @Override
   public Holding[] getAllHoldings()
   {
      return library.getAllHoldings();
   }

   @Override
   public int countBooks()
   {
      return library.countBooks();
   }

   @Override
   public int countVideos()
   {
      return library.countVideos();
   }

   @Override
   public void borrowHolding(int holdingId) throws InsufficientCreditException,
            MultipleBorrowingException
   {
      library.borrowHolding(holdingId);  
   }

   @Override
   public void returnHolding(int holdingId) throws OverdrawnCreditException
   {
      library.returnHolding(holdingId);
   }

   @Override
   public HistoryRecord[] getBorrowingHistory()
   {
      return library.getBorrowingHistory();
   }

   @Override
   public HistoryRecord getHistoryRecord(int holdingId)
   {
      return library.getHistoryRecord(holdingId);
   }

   @Override
   public Holding[] getBorrowedHoldings()
   {
      return library.getBorrowedHoldings();
   }

   @Override
   public void resetMemberCredit()
   {
      library.resetCredit();      
   }

   @Override
   public int calculateRemainingCredit()
   {
      return library.calculateRemainingCredit();
   }

   @Override
   public int calculateTotalLateFees()
   {
      return library.calculateTotalLateFees();
   }
   
}